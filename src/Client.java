/**
 * WEEK 04 - JSON and HTTP or URL
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-03
 * <p>
 * Write an application with two separate components.
 * The first is an HTTP server component that when called takes an object and converts it to JSON
 * using the Jackson JSON parser, and returns a block of JSON using HTTP.
 * The second component is a client component.
 * It makes a call to the server component, gets the JSON it returns, and then converts that using
 * the Jackson JSON parser, into an object.
 * These two components need to be written as two separate classes.
 * Make sure you incorporate error handling and data validation into your programs,
 * including catching the most specific type of exceptions you can.
 * Use this sequence diagram to help you understand what is required.
 */

public class Client {

    private static final String WHAT_PROGRAM_DOES_MSG = "THIS PROGRAM CONVERTS JSON TO OBJECT AND VICE-VERSA BY USING REMOTE AND LOCAL HTTP SERVER.";
    private static final String REMOTE_HTTP_SERVER = "https://api.openweathermap.org/data/2.5/weather?q=Rexburg&apiKey=585fb70864d3699f0df81986d3fce1d9&units=imperial";
    private static final String LOCAL_HTTP_SERVER = "http://localhost:8080/";
    private static final String START_LOCAL_SERVER_MSG = "\nStarting the local HTTP Server and publishing data...";
    private static final String STOP_LOCAL_SERVER_MSG = "\nStopping the local HTTP Server...";
    private static final String JSON_TO_OBJECT_MSG = "\nCONVERTING JSON OBJECT TO WEATHER CONDITION OBJECT (only important info):\n";
    private static final String OBJECT_TO_JSON_MSG = "\nSERIALIZING THE PREVIOUS WEATHER CONDITION OBJECT TO JSON FORMAT:\n";
    private static final String REXBURG_REMOTE_MSG = "\nJSON OBJECT RECEIVED FROM OPENWEATHERMAP.ORG:\n";
    private static final String REXBURG_LOCAL_MSG = "\nJSON OBJECT RECEIVED FROM LOCAL HTTP SERVER:\n";
    private static final String CONVERTING_JSON_TO_OBJECT_MSG = "\nCONVERTING THE PREVIOUS JSON OBJECT TO WEATHER CONDITION OBJECT:\n";

    private static final String GOODBYE_MSG = "\nGoodbye ...";


    public static void main(String[] args) {
        // ********************************* HEADER **********************************
        // Display the header
        System.out.println(WHAT_PROGRAM_DOES_MSG);
        // Create and instance of Http Server
        Server httpServer = new Server();


        // ******************* COLLECTING DATA FROM PUBLIC WEB API *******************
        // I first thought we should bring data from an API. Since I spent some time
        // doing this part of the work, I decided to integrate it in the assignment
        // because this part of the code will be very useful in our project.
        httpServer.setUrlString(REMOTE_HTTP_SERVER);

        // Receive JSON object from https://api.openweathermap.org
        String jsonFromWeb = httpServer.getJsonContent();
        System.out.println(REXBURG_REMOTE_MSG + jsonFromWeb);
        // Result: JSon object is received from the WEB and displayed on the screen.

        // Deserialize JSON format to Weather Conditions Object
        WeatherConditions weatherFromWeb = new WeatherConditions().JSONToWeatherCondition(jsonFromWeb);
        System.out.println(JSON_TO_OBJECT_MSG + weatherFromWeb);
        // Result: JSON is converted to Weather object and displayed on the screen.


        // ******************* COLLECTING DATA FROM LOCAL HTTP SERVER ****************
        httpServer.setUrlString(LOCAL_HTTP_SERVER);

        // Serialize Weather Conditions Object to JSON format
        String jsonFromObject = weatherFromWeb.weatherConditionToJSON(weatherFromWeb);
        System.out.println(OBJECT_TO_JSON_MSG + jsonFromObject);
        // Result: Weather object is converted to JSON and displayed on the screen.

        // Start local server
        httpServer.setDataContent(jsonFromObject);
        System.out.println(START_LOCAL_SERVER_MSG);
        httpServer.startLocalServer();
        // Result: Local HTTP Server is started and content is published.

        // Receive JSON object from http://localhost:8080
        String jsonFromLocalServer = httpServer.getJsonContent();
        System.out.println(REXBURG_LOCAL_MSG + jsonFromLocalServer);
        httpServer.stopLocalServer();
        System.out.println(STOP_LOCAL_SERVER_MSG);
        // Result: JSon object is received from local HTTP Server and displayed on the screen.

        // Deserialize JSON format to Weather Conditions Object
        WeatherConditions weatherFromLocalServer = new WeatherConditions().JSONToWeatherCondition(jsonFromLocalServer);
        System.out.println(CONVERTING_JSON_TO_OBJECT_MSG + weatherFromLocalServer);
        // Result: JSON is converted to Weather object and displayed on the screen.


        // ******************************** END PROGRAM ********************************
        // Display Goodbye message
        System.out.println(GOODBYE_MSG);
    }
}
