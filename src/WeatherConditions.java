import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * WEATHER CONDITIONS
 * This class defines the Weather Conditions
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-02
 */
// I used the following tag in order to ignore data from JSON object that is not mapped in this class.
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherConditions {

    private static final String ERROR_PARSING_MSG = "There was a problem while parsing the object.";

    @JsonProperty("main") private Map<String, Float> measurements;
    @JsonProperty("sys") private Map<String, String> system;
    private String name;
    private String id;
    private String capital;

    /**
     * Default Weather Conditions constructor
     */
    public WeatherConditions() {
        this.id = null;
        this.name = null;
        this.capital = null;
        this.measurements = null;
        this.system = null;
    }

    /**
     * This function gets the city ID.
     *
     * @return the id of the city.
     */
    public String getId() {
        return id;
    }

    /**
     * This funciton sets the city ID.
     *
     * @param id id of the City.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * This function gets the name of the city.
     *
     * @return the city's name
     */
    public String getName() {
        return name;
    }

    /**
     * This function sets the name of the city.
     *
     * @param name is the name of the city.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This function gets the map inside the main nested block.
     *
     * @return a map of the main nested block.
     */
    public Map<String, Float> getMeasurements() {
        return measurements;
    }

    /**
     * This function sets the values inside the nested main block.
     *
     * @param measurements the measures of the weather.
     */
    public void setMeasurements(Map<String, Float> measurements) {
        this.measurements = measurements;
    }

    /**
     * This function gets the map inside the sys nested block
     *
     * @return a map of the sys nested block.
     */
    public Map<String, String> getSystem() {
        return system;
    }

    /**
     * This function sets the values inside the nested sys block.
     *
     * @param system info of the city
     */
    public void setSystem(Map<String, String> system) {
        this.system = system;
    }

    /**
     * This function gets the capital of the city.
     *
     * @return the capital of the city.
     */
    public String getCapital() {
        return capital;
    }

    /**
     * This function sets the capital of the city.
     *
     * @param capital the capital's name
     */
    public void setCapital(String capital) {
        this.capital = capital;
    }

    /**
     * This function return the measurements conditions from the nested main block.
     *
     * @param attribute to be retrieved
     * @return measurements data
     */
    public String getCurrentCondition(String attribute) {
        return this.measurements.get(attribute).toString();
    }

    /**
     * This function return the system info from the nested sys block.
     *
     * @param attribute to be retrieved
     * @return system data
     */
    public String getCurrentSystem(String attribute) {
        return this.system.get(attribute).toString();
    }

    /**
     * This function converts a Weather Condition object to JSON object.
     *
     * @param weatherConditions object
     * @return the JSON object
     */
    public String weatherConditionToJSON(WeatherConditions weatherConditions) {

        ObjectMapper mapper = new ObjectMapper();
        String json = null;

        try {
            json = mapper.writeValueAsString(weatherConditions);
        } catch (JsonProcessingException e) {
            System.out.println(ERROR_PARSING_MSG);
        }

        return json;
    }

    /**
     * This function converts a JSON object to Weather Condition object.
     *
     * @param json object
     * @return the Weather Conditions object
     */
    public WeatherConditions JSONToWeatherCondition(String json) {

        ObjectMapper mapper = new ObjectMapper();
        WeatherConditions weatherConditions = null;

        try {
            weatherConditions = mapper.readValue(json, WeatherConditions.class);
        } catch (JsonProcessingException e) {
            System.out.println(ERROR_PARSING_MSG);
        }

        return weatherConditions;
    }

    /**
     * This function displays the weather conditions of the city.
     */
    public String toString() {
        return "Current weather condition in: " + this.name + "\n" +
                "The temperature is: " + getCurrentCondition("temp") + " F" + "\n" +
                "The feels like is: " + getCurrentCondition("feels_like") + " F" + "\n" +
                "The pressure is: " + getCurrentCondition("pressure") + " hpa" + "\n" +
                "The humidity is: " + getCurrentCondition("humidity") + " %";
    }
}
