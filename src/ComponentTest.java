import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * SERVERS Tests
 * This class defines the test cases to validate the JSON published and collected from PUBLIC WEB API and local HTTP Server.
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-10
 */
public class ComponentTest {

    private static Server httpServer;
    private static WeatherConditions weatherFromWeb;
    private static WeatherConditions weatherFromLocalServer;
    private static String jsonFromObject;
    private static String jsonFromLocalServer;
    private static final String REXBURG_CITY = "Rexburg";
    private static final String REXBURG_ID = "5605242";
    private static final String LOCAL_HTTP_SERVER = "http://localhost:8080/";
    private static final String API_URL = "https://api.openweathermap.org/data/2.5/weather?q=";
    private static final String API_KEY = "&apiKey=585fb70864d3699f0df81986d3fce1d9";
    private static final String API_UNIT = "&units=imperial";

    /**
     * This method will run before the test cases starts in order to collect and retrieve data from LOCAL and REMOTE servers.
     */
    @BeforeAll
    static void initAll() {
        // ******************* COLLECTING DATA FROM PUBLIC WEB API *******************
        // Create and instance of Http Server
        httpServer = new Server();
        httpServer.setUrlString(API_URL + REXBURG_CITY + API_KEY + API_UNIT);
        String jsonFromWeb = httpServer.getJsonContent();
        weatherFromWeb = new WeatherConditions().JSONToWeatherCondition(jsonFromWeb);
        // Result: JSON from WEB API is converted to Weather Conditions object.

        // ******************* PUBLISHING DATA TO LOCAL HTTP SERVER ****************
        // Start local server
        httpServer.setUrlString(LOCAL_HTTP_SERVER);
        jsonFromObject = weatherFromWeb.weatherConditionToJSON(weatherFromWeb);
        httpServer.setDataContent(jsonFromObject);
        httpServer.startLocalServer();
        // Result: Local HTTP Server is started and content retrieved from WEB API is published to local HTTP Server.

        // ******************* COLLECTING DATA FROM LOCAL HTTP SERVER ****************
        // Receive JSON object from http://localhost:8080
        jsonFromLocalServer = httpServer.getJsonContent();
        weatherFromLocalServer = new WeatherConditions().JSONToWeatherCondition(jsonFromLocalServer);
        // Result: JSON is converted to Weather object and displayed on the screen.
    }

    /**
     * This method will run after all test cases is executed in order to close the local HTTP Server.
     */
    @AfterAll
    static void tearDownAll() {
        // Closing the instance of Http Server
        httpServer.stopLocalServer();
        // Result: HTTP Server is no longer available
    }

    @Test
    /**
     * This test case checks whether the cities names collected from WEB API is the same found in the local HTTP Server.
     */
    void validateCitiesNames() {
        assertEquals(weatherFromWeb.getName(), weatherFromLocalServer.getName());
        assertEquals(weatherFromWeb.getId(), weatherFromLocalServer.getId());
        // Result: BOTH cities names from WEB API and LOCAL HTTP server are the same.
    }

    @Test
    /**
     * This test case checks whether the city name collected from WEB API and HTTP Server are well formatted.
     */
    void validateCityFormatName() {
        assertFalse(REXBURG_CITY == weatherFromWeb.getName().toUpperCase());
        assertFalse(REXBURG_CITY == weatherFromWeb.getName().toLowerCase());
        assertFalse(REXBURG_CITY == weatherFromLocalServer.getName().toUpperCase());
        assertFalse(REXBURG_CITY == weatherFromLocalServer.getName().toLowerCase());
        // Result: BOTH cities names are not all upper or lower case from both servers.
    }

    @Test
    /**
     * This test case checks whether the measurement contents retrieved from both Servers are not null.
     */
    void validateMeasurementReturnNotNull() {
        // From WEB API Server
        assertNotNull(weatherFromWeb.getCurrentCondition("temp"));
        assertNotNull(weatherFromWeb.getCurrentCondition("feels_like"));
        assertNotNull(weatherFromWeb.getCurrentCondition("pressure"));
        assertNotNull(weatherFromWeb.getCurrentCondition("humidity"));
        assertNotNull(weatherFromWeb.getCurrentCondition("temp_min"));
        assertNotNull(weatherFromWeb.getCurrentCondition("temp_max"));
        // Result: The content measurement retrieved from WEB API Server are not null.

        // From LOCAL HTTP Server
        assertNotNull(weatherFromLocalServer.getCurrentCondition("temp"));
        assertNotNull(weatherFromLocalServer.getCurrentCondition("feels_like"));
        assertNotNull(weatherFromLocalServer.getCurrentCondition("pressure"));
        assertNotNull(weatherFromLocalServer.getCurrentCondition("humidity"));
        assertNotNull(weatherFromLocalServer.getCurrentCondition("temp_min"));
        assertNotNull(weatherFromLocalServer.getCurrentCondition("temp_max"));
        // Result: The content measurement retrieved from LOCAL HTTP Server are not null.
    }

    @Test
    /**
     * This test case checks whether the temp min is not higher than temp max and that they are equals from both Servers.
     */
    void validateMinMaxTempRange() {

        // From WEB API Server
        double tempMin_FromWeb = Double.parseDouble(weatherFromWeb.getCurrentCondition("temp_min"));
        double tempMax_FromWeb = Double.parseDouble(weatherFromWeb.getCurrentCondition("temp_max"));
        assertTrue(tempMin_FromWeb <= tempMax_FromWeb);
        // Result: tempMin must not be higher than tempMax from WEB API Server

        // From LOCAL HTTP Server
        double tempMin_HttpServer = Double.parseDouble(weatherFromLocalServer.getCurrentCondition("temp_min"));
        double tempMax_HttpServer = Double.parseDouble(weatherFromLocalServer.getCurrentCondition("temp_max"));
        assertTrue(tempMin_HttpServer <= tempMax_HttpServer);
        // Result: tempMin must not be higher than tempMax from WEB API Server
    }

    @Test
    /**
     * This test case checks whether the city and country name are not the same. It checks from both servers.
     */
    void validateCityAndCountryNotSameData(){
        assertNotSame(weatherFromWeb.getName(), weatherFromWeb.getCurrentSystem("country"));
        assertNotSame(weatherFromLocalServer.getName(), weatherFromWeb.getCurrentSystem("country"));
        // Result: city and country fields name must hold the same content.
    }

    @Test
    /**
     * This test case checks whether the instance returned from weatherFromLocalServer is the same.
     */
    void validateJsonPublishedAndCollected(){
        assertSame(weatherFromLocalServer.getCurrentSystem("country"), weatherFromLocalServer.getCurrentSystem("country"));
        // Result: Broth objects results must come from the same instance. It is based on the same object ID.
    }

    @Test
    /**
     * This test case checks whether the capital field is null because this field was not implemented in the WEB API Json.
     */
    void validateCapitalFieldAsNull(){
        assertNull(weatherFromWeb.getCapital());
        assertNull(weatherFromLocalServer.getCapital());
        // Result: the capital was not implemented in the WEB API Json.
    }

    @Test
    /**
     * This function checks whether the content de deserialized from the WEB API Object is the same published and collected from HTTP Server.
     */
    void validateContentPublishedAndReceived(){
        assertThat(jsonFromObject, equalTo(jsonFromLocalServer));
        // Result: The json content deserialized from the WEB API Object must be the same published and collected from HTTP Server.
        // PS, assertThat is deprecated.
    }

    @Test
    /**
     * This test case checks whether the city ID (converted to an array) is the same retrieved from both servers.
     */
    void validateCityID(){
        String[] cityId = REXBURG_ID.split("");
        String[] weatherFromWebID = weatherFromWeb.getId().split("");
        String[] weatherFromLocalServerID = weatherFromLocalServer.getId().split("");

        assertArrayEquals(cityId, weatherFromWebID);
        assertArrayEquals(cityId, weatherFromLocalServerID);
        // Result: It converts the city ID to an array and compare it to the city array from each server
    }
}
