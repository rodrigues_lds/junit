import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.ServerError;

/**
 * HTTP / URL SERVER
 * This class defines the Weather Conditions
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-02
 */

public class Server {

    // Set the default messages and values
    private static final String PROCESSING_ERROR_MSG = "There was an error while processing the content.";
    private static final String GENERAL_ERROR_MSG = "Something went wrong!";
    private static final String SERVER_NOT_STARTED_MSG = "The server could not be started.";
    private static final String DATA_FAILED_MSG = "Could not send data to the server";
    private static final String SERVER_NOT_STOPPED_MSG = "There was a problem while stopping the server.";

    private HttpServer server;
    private String urlString;
    private static String dataContent;

    /**
     * Default Server constructor
     */
    public Server() {
        urlString = null;
    }

    /**
     * This function gets the URL String - the HTTP address.
     *
     * @return the URL/HTTP address
     */
    public String getUrlString() {
        return urlString;
    }

    /**
     * This function sets the URL String - the HTTP address.
     *
     * @param urlString is the target
     */
    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    /**
     * This function gets the content to be published in the server.
     *
     * @return the content
     */
    public String getDataContent() {
        return dataContent;
    }

    /**
     * This function sets the content to be published in the server.
     *
     * @param dataContent to be published
     */
    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    /**
     * This function start the HTTP Server.
     */
    public void startLocalServer() {
        server = null;
        try {
            // Creating the localhost server at port 8080
            server = HttpServer.create(new InetSocketAddress(8080), 0);

            // Setting the path of the server. In this project, no path will be used.
            HttpContext context = server.createContext("/");
            // sending the content by calling sendContent function.
            context.setHandler(Server::sendContent);
            server.start();
            // Result: the server has started.

        } catch (ServerError ex) {
            System.out.println(SERVER_NOT_STARTED_MSG);
        }catch (IOException e) {
            System.out.println(DATA_FAILED_MSG);
        }
    }

    /**
     * This function stops the HTTP Server.
     */
    public void stopLocalServer(){
        try {
            server.stop(0);
        }catch (Exception ex){
            System.out.println(SERVER_NOT_STOPPED_MSG);
        }
    }

    /**
     * This function sends the content to the target.
     *
     * @param exchange is the response to be generated in on exchange. It builds the sending response.
     * @throws IOException if the data was not properly written in the HTTP target.
     */
    private static void sendContent(HttpExchange exchange) throws IOException {

        String content = dataContent;

        // Response code and length
        exchange.sendResponseHeaders(200, content.getBytes().length);

        // Accepting output bytes and sending them to the target (urlString)
        OutputStream os = exchange.getResponseBody();
        os.write(content.getBytes());
        os.close();
        // Result: Data sent/written to the target.
    }

    /**
     * This function gets contents from the remote (Web) or local (Http Server).
     *
     * @return a string with JSON object.
     */
    public String getJsonContent() {

        String jsonString = null;

        try {
            // Prepare the connection
            URL url = new URL(this.urlString);
            URLConnection conn = url.openConnection();
            // Result: the connection is opened to the target (urlString)

            // Retrieve the content
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            jsonString = reader.readLine();
            // Result: Since the contents are based on API / JSON, only a single line is returned.

        } catch (JsonProcessingException ex) {
            // If there is an error while processing the JSON.
            System.out.println(PROCESSING_ERROR_MSG);
        } catch (Exception ex) {
            // If a general error is identified.
            System.out.println(GENERAL_ERROR_MSG);
        }

        // Assignment Requirement: returns a block of JSON
        return jsonString;
    }
}